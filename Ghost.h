#pragma once

//parent class
#include "ScreenObject.h"

//other dependencies
#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <cstdio>
#include <string>

class Ghost : public ScreenObject {
public:
Ghost() {};
Ghost(glm::vec2 pos, glm::vec2 sz); //size 

void Ghost_calcVelocity();					//calculate ghost velocity
void Ghost_updatePosition(float deltaTime);	//change ghosts position
void Ghost_updateDirection(); 				//change ghosts direction				



private:
	glm::vec2  Ghost_velocity;
	glm::vec2  Ghost_direction;
	glm::vec2  Ghost_position; 
	int ghost_speed;

};