#pragma	once

#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <cstdio>
#include <string>

//Mother class of all objects on our screen
class ScreenObject {
public:
	ScreenObject() {};			//default constructor, not in use
	ScreenObject(glm::vec2 pos, glm::vec2 sz) : position(pos), size(sz) {};	//constructor for ScreenObject
	~ScreenObject() {};

	glm::vec2 getPosition();
	glm::vec2 getCentrePos(); 
	void setPosition(glm::vec2 temp);			//sets position in world
	SDL_Rect getSourceRect();					//gets source SDL_rect for sprites
	SDL_Rect getDestRect();						//gets destination SDL_rect for our draw calls
	void setSpritePos(int x, int y);			//sets where in out spritesheet our sprite is
	bool collisionDetection(glm::vec2 temp);	//checks for collision v1
	glm::vec2 getSize();						
	bool collisionDetection(glm::vec2 temp1, glm::vec2 temp2);	//checks for collision v2 (not in use atm, to be improved)
	bool centerCollision(glm::vec2 temp);						//check for collision around the center of a object
	void setSpriteSize(glm::vec2 temp);							//sets size of sprite in spritesheet


protected:
	glm::vec2 position;
	glm::vec2 size;
	glm::vec2 spritePosition;			//Position of sprite on the spriteSheet
	int blit;
	float nextTime;
	bool reverse;
	glm::vec2 spriteSize;
};