# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc


Members of this group:
Stdnumber: 470901
Name: Bjørn Kaare Aune
Email: bjornkau@stud.ntnu.no

Name: Benjamin Skinstad	
Email: benski@live.no	

##Group comments
###Who are the members of the group?###
###What did you implement and how did you do it? (Individually)###
###What parts if any of the base code did you change and why?###
###What was the hardest part of this assignment? (Individually)###
###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
###Other comments###
