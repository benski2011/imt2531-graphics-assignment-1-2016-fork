#include <SDL2/SDL.h>
#include "InputHandler.h"
#include "globals.h"

void InputHandler::init() {
	//Add new events here and in GameEvent.h
	eventRepeatRate[ActionEnum::QUIT] = 0;
	eventRepeat[ActionEnum::QUIT] = INACTIVE;

	eventRepeatRate[ActionEnum::MOUSEMOTION] = 0;
	eventRepeat[ActionEnum::MOUSEMOTION] = INACTIVE;

	eventRepeatRate[ActionEnum::PLAYER_MOVE_UP] = 0;
	eventRepeat[ActionEnum::PLAYER_MOVE_UP] = INACTIVE;

	eventRepeatRate[ActionEnum::PLAYER_MOVE_DOWN] = 0;
	eventRepeat[ActionEnum::PLAYER_MOVE_DOWN] = INACTIVE;

	eventRepeatRate[ActionEnum::PLAYER_MOVE_LEFT] = 0;
	eventRepeat[ActionEnum::PLAYER_MOVE_LEFT] = INACTIVE;

	eventRepeatRate[ActionEnum::PLAYER_MOVE_RIGHT] = 0;
	eventRepeat[ActionEnum::PLAYER_MOVE_RIGHT] = INACTIVE;

	eventRepeatRate[ActionEnum::PLAYER_MUTE_MUSIC] = 0;
	eventRepeat[ActionEnum::PLAYER_MUTE_MUSIC] = INACTIVE;

	eventRepeatRate[ActionEnum::CHANGE_WALL_ONE] = 0;
	eventRepeat[ActionEnum::CHANGE_WALL_ONE] = INACTIVE;

	eventRepeatRate[ActionEnum::CHANGE_WALL_TWO] = 0;
	eventRepeat[ActionEnum::CHANGE_WALL_TWO] = INACTIVE;

	eventRepeatRate[ActionEnum::CHANGE_WALL_THREE] = 0;
	eventRepeat[ActionEnum::CHANGE_WALL_THREE] = INACTIVE;

	eventRepeatRate[ActionEnum::CHANGE_WALL_DISCO] = 0;
	eventRepeat[ActionEnum::CHANGE_WALL_DISCO] = INACTIVE;
}

void InputHandler::readInput(std::queue<GameEvent>& events) {
	//Handle new events on queue
	while (SDL_PollEvent(&event) != 0) {
		//User requests quit
		if (event.type == SDL_QUIT) {
			gRunning = false;
		} else if (	event.type == SDL_KEYDOWN ||
					event.type == SDL_KEYUP)
		{
			handleKeys(events);
		} else if (	event.type == SDL_MOUSEBUTTONDOWN ||
					event.type == SDL_MOUSEBUTTONUP ||
					event.type == SDL_MOUSEMOTION ||
					event.type == SDL_MOUSEWHEEL)
		{
			handleMouse(events);
		}
	}
	
	//This will trigger all events that have finnished cool down
	//This code is currently not connected to deltaTime. Considder doing so.
	for (auto it = eventRepeat.begin(); it != eventRepeat.end(); ++it) {
		if (it->second != INACTIVE) {
			if (it->second == 0) {
				events.push(GameEvent{ 0, it->first }); // raise the event
				it->second = eventRepeatRate[it->first];  // set to cool down
			} else {
				--it->second;  // cool down toward triggering
			}
		}
	}
}

void InputHandler::handleKeys(std::queue<GameEvent>& events) {
	ActionEnum action = ActionEnum::NOACTION;
	switch (event.type) {
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
				case SDLK_ESCAPE:
				{
					gRunning = false;
					break;
				}
				case SDLK_w:
				{
					action = ActionEnum::PLAYER_MOVE_UP;
					break;
				}
				case SDLK_s:
				{
					action = ActionEnum::PLAYER_MOVE_DOWN;
					break;
				}
				case SDLK_a:
				{
					action = ActionEnum::PLAYER_MOVE_LEFT;
					break;
				}
				case SDLK_d:
				{
					action = ActionEnum::PLAYER_MOVE_RIGHT;
					break;
				}
				case SDLK_m:
				{
					action = ActionEnum::PLAYER_MUTE_MUSIC;
					break;
				}
				case SDLK_1:
				{
					action = ActionEnum::CHANGE_WALL_ONE;
					break;
				}
				case SDLK_2:
				{
					action = ActionEnum::CHANGE_WALL_TWO;
					break;
				}
				case SDLK_3:
				{
					action = ActionEnum::CHANGE_WALL_THREE;
					break;
				}
				case SDLK_4:
				{
					action = ActionEnum::CHANGE_WALL_DISCO;
					break;
				}
				default:
					break;
			}

			//This checks if the event is active and on cool down
			if (eventRepeat[action] < 0)
			{
				events.push(GameEvent{ 0, action }); //trigger event
				eventRepeat[action] = eventRepeatRate[action]; // set the repeat cool down
			}
			break;

		case SDL_KEYUP:
			switch (event.key.keysym.sym) {
				default:
					break;
			}

			eventRepeat[action] = INACTIVE;
			break;

		default:
			break;
	}
	eventRepeat[action] = INACTIVE;
}
void InputHandler::handleMouse(std::queue<GameEvent>& events) {
    ActionEnum action = ActionEnum::NOACTION;
    
    switch (event.type)
    {
        case SDL_MOUSEBUTTONDOWN:
            break;
        case SDL_MOUSEBUTTONUP:
            break;
        case SDL_MOUSEWHEEL:
            break;
        case SDL_MOUSEMOTION:
            mousePosition.x = event.motion.x;
            mousePosition.y = event.motion.y;
            action = ActionEnum::MOUSEMOTION;
            break;
    }

    if (eventRepeat[action] < 0) {
        events.push(GameEvent{ 0, action }); //trigger event
        eventRepeat[action] = eventRepeatRate[action]; // set the repeat cool down
    }
}