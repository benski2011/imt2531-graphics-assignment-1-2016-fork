#pragma once 

#include <string>
#include <SDL2/SDL.h>
#include <vector>
#include <glm/glm.hpp>

#include "WindowHandler.h"
#include "ScreenObject.h"

class TextHandler 
{
public:
	static TextHandler& getInstance() {
		static TextHandler instance;
		return instance;
	}
	TextHandler();
	~TextHandler() {};

	void drawText(std::string s);		//handles text

private:
	std::string letters;	
	glm::vec2 letterSz;
	int lines;
};