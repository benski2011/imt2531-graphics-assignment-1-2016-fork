#include "Level.h"
#include "WindowHandler.h"

Level::Level(std::string filepath)
{
	playerObj = nullptr;
	loadMap(filepath);
	createWalls();
	createPlayer();
	createCandy(); 
	createGhost();
	Music = Mix_LoadMUS("music.wav");
	musicPlaying = true;
	discoMode = false;
	nextTime = 0;
	currentWalls = 8;

}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath)
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	for(int i = 0; i<y; i++) {
		std::vector<int> row;
		for(int j = 0; j<x; j++) {
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}



std::vector<ScreenObject>* Level::getWalls()
{
	return &walls;
}

std::vector<ScreenObject>* Level::getCandy() 
{
	return &candys; 
}
std::vector<ScreenObject>* Level::getGhost() 
{
	return &ghosts; 
}

std::vector<ScreenObject>* Level::getGameObj()
{
	return &gameObj;
}

ScreenObject* Level::getPlayerObj()
{
	return    (ScreenObject*) playerObj;
}

PlayerObject* Level::getPlayer()
{
	return playerObj;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createWalls()
{
	glm::vec2 pos, size, spriteSize;

	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++)
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++)
		{
			if(map[i][j] == 1)
			{
				int random=rand()%4; 
				pos.x = j * 720/36; pos.y = i * 720/36;
				size.x = 720/36; size.y = 720/36;
				spriteSize.x = 80;
				spriteSize.y = 80;
				ScreenObject wall(pos, size);
				wall.setSpriteSize(spriteSize);
				wall.setSpritePos(80*8, 80*1);
				walls.push_back(wall);									
			}

		}
	}
}
//same as for createWalls, but for candy objects
void Level::createCandy()
{
	glm::vec2 pos, size, spriteSize;
	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++)
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++)
		{
			int randomCandy=rand()%4;
			if(map[i][j] == 3)
			{
				pos.x = j * 720/36; pos.y = i * 720/36;
				size.x = 720/36; size.y = 720/36;
				spriteSize.x = 80;
				spriteSize.y = 80;
				ScreenObject candy(pos, size);
				candy.setSpriteSize(spriteSize);
				candy.setSpritePos(80*11, 80*randomCandy);
				candys.push_back(candy);									
			}
		}
	}
}


//same as for walls and candy, but for the playerobject
void Level::createPlayer()
{
	glm::vec2 pos, size, spriteSize;
	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++)
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++)
		{
			if(map[i][j] == 2)
			{
				pos.x = j * 720/36; pos.y = i * 720/36;
				size.x = 720/36; size.y = 720/36;
				spriteSize.x = 80;
				spriteSize.y = 80;
				playerObj = new PlayerObject(pos, size);
				playerObj->setSpritePos(0, 0);
				playerObj->setSpriteSize(spriteSize);
				//playerObj->setGridPos(i,j);										
			}
		}
	}
}
//creates Ghosts in the same way as for all other game objects
void Level::createGhost()
{
	int ghostColor=0; 
	glm::vec2 pos, size, spriteSize;
	int rows = map.size();
	int cells = 0;

	for(int i = 0; i<rows; i++)
	{
		cells = map[i].size();

		for(int j = 0; j<cells; j++)
		{
			if(map[i][j] == 7)
			{
				pos.x = j * 720/36; pos.y = i * 720/36;
				size.x = 720/36; size.y = 720/36;
				spriteSize.x = 80;
				spriteSize.y = 80;
				ScreenObject Ghost(pos, size);
				Ghost.setSpriteSize(spriteSize);
				Ghost.setSpritePos(80*4+(80*ghostColor), 0);
				ghostColor++;

				ghosts.push_back(Ghost);
				
													
			}
		}
	}
}

//updates level.
void Level::updateLevel(float dt)
{
	if(Mix_PlayingMusic() == 0 && musicPlaying == true)	//if music is toggled, play it
	{
		Mix_PlayMusic(Music, -1);
	}
	else if(Mix_PlayingMusic() == 1 && musicPlaying == false) //else, mute it
	{
		Mix_HaltMusic();
	}

	collisionDetection();			//checks for all events that need collision
	playerObj->updatePosition(dt);	//updates pacman's velocity


	if (discoMode)		//if dicomode enabled, updates sprites fast
	{
		nextTime += dt;
		if (0.2 <= nextTime)
		{
			changeWallSprites(currentWalls);
			nextTime = 0.f;
		}
	}
}

//changes pacmans direction.
void Level::updateDirection(char c)
{
	playerObj->updateDirection(c);
}


//checking for collision between pacman and all other objects. 
//can be expanded to include ghosts.
void Level::collisionDetection()
{
	glm::vec2 pacCenter, size, testPos1, testPos2, tempDir;
	pacCenter = playerObj->getPosition();
	size = playerObj->getSize();
	pacCenter.x += (size.x/2);
	pacCenter.y += (size.y/2);
	tempDir = playerObj->getDirection();
	//calculating first collision point on packman
	testPos1.x = pacCenter.x + ((size.x/2)*tempDir.x);
	testPos1.y = pacCenter.y + ((size.y/2)*tempDir.y);
	
	//testPos2.x = pacCenter.x + ((size.x/2)*tempDir.x) - ((size.x/4)*tempDir.x);
	//testPos2.y = pacCenter.y + ((size.y/2)*tempDir.y) - ((size.y/4)*tempDir.y);


	//checks for collision between pacman and walls
	for(int i = 0; i < walls.size(); i++)
	{
		if(walls[i].collisionDetection(testPos1))
		{
			playerObj->setSpeedZero();
		}
	}

	//checks for collision between candy and pacman
	for(int i = 0; i < candys.size(); i++)
	{
		if(candys[i].collisionDetection(pacCenter))
		{
			candys.erase(candys.begin()+i);
			playerObj->updateScore();
		}
	}
	/*glm::vec2 pacPos, pacDir;

	pacPos = playerObj->getPosition();
	pacDir = playerObj->getDirection();
	pacPos.x = pacPos.x * 720/28;
	pacPos.y = pacPos.y * 720/36;
	if()*/

}

//flips music bool
void Level::muteSound()
{
	musicPlaying = !musicPlaying;
}

//disco mode
void Level::changeWallSprites(int n =8)
{
	int random;
	for(int i = 0; i < walls.size(); i++)
	{
		random = rand()%4;
		walls[i].setSpritePos(80*n, 80*random);
	}
	currentWalls = n;

}

//flips disco bool
void Level::enableDisco()
{
	discoMode = !discoMode;
}

//will implement warpspaces.
//Not sure if we have the time to fix this
/*if(map[i][j] == 1)
{
	pos.x = j * 720/36; pos.y = i * 720/36;
	size.x = 720/36; size.y = 720/36;
	spriteSize.x = 80;
	spriteSize.y = 80;
	ScreenObject wall(pos, size);
	wall.setSpriteSize(spriteSize);
	wall.setSpritePos(80*8, 80);
	//wall.setGridPos(i,j);	
	walls.push_back(wall);									
}*/
