#include "Ghost.h"

//Default constructor for Ghost objects
Ghost::Ghost(glm::vec2 pos, glm::vec2 sz) : ScreenObject(pos, sz)
{
	ghost_speed = 100;
	Ghost_direction.x = 0;
    Ghost_direction.y = 1;
	blit = 0;
	nextTime = 0.f;
	reverse = false;
	Ghost_calcVelocity();
}
//Calculating ghosts velocity
void Ghost::Ghost_calcVelocity()
{
	 Ghost_velocity.x = ghost_speed *  Ghost_direction.x;
	 Ghost_velocity.y = ghost_speed *  Ghost_direction.y;
}
//updates ghosts position
void Ghost::Ghost_updatePosition(float deltaTime)
{
	nextTime += deltaTime;
	
	 Ghost_position.x +=  (Ghost_velocity.x * deltaTime)+10;
	 Ghost_position.y +=  (Ghost_velocity.y * deltaTime)+10;
}
//This will handle movement changes for ghost (not fully implemented)
void Ghost::Ghost_updateDirection()
{	
 Ghost_calcVelocity();
}