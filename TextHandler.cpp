#include "TextHandler.h"
#include <string>

//Inits texthandler, puts all letter positions into array/string/vector -- KB
TextHandler::TextHandler()
{

	 letterSz.x = 22;
	 letterSz.y = 22;
	 lines = 0;
	 //We are only adding the symbols we are going to use, but the principles used will work for all symbols on the
	 //text sprite sheet
	 letters = ("!\"#$\%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghjiklmnopqrstuvwxyz{|}~ ");
}

//gets a string, and draws is to the screen. 
//gets information on where to find sprites based on position of char in letters
//calls a WindowHandler.draw(screenObject*) to draw text to our screen
void TextHandler::drawText(std::string s)
{
	
	std::string tempString = s;
	
	glm::vec2 tempStartPos, tempSize;
	tempStartPos.x = 0;
	tempStartPos.y = 0;
	int search, xSheetPos, ySheetPos;
	tempSize.x = 21;
	tempSize.y = 25;
	//compares string sent in with the letters string, draws string to screen
	for(int i = 0; i < s.length(); i++)
	{
		
		for(int j = 0; j < letters.length(); j++)
		{
		
			if(tempString[i] == letters[j])
			{
				search = j;
				//printf("Search = %i\n", search);
				xSheetPos = (search % 20)*21;
				ySheetPos = (search / 20)*25;
				tempStartPos.x += 22; //Magic number GO(fix this) --KB
				ScreenObject* temp;
				temp = new ScreenObject(tempStartPos,tempSize);
				temp->setSpritePos(xSheetPos, ySheetPos);
				temp->setSpriteSize(letterSz);
				WindowHandler::getInstance().drawText(temp);
				delete temp;
			}
		}
	}
}