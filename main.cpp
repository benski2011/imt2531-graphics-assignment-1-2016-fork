#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>
#include <SDL2/SDL_mixer.h>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "globals.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Level.h"

//Creates a Level object for each line in gLEVELS_FILE andplace it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels() {
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++)
	{
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

//Initializes the InputHandler and WindowHandler
//The WindowHandler intitilizes SDL
//Loads levels and returns a pointer to the first Level by calling loadLevels()
Level* init()
{
	Mix_OpenAudio(22050,MIX_DEFAULT_FORMAT,2,4096);
	
	InputHandler::getInstance().init();
	if(!WindowHandler::getInstance().init()) {
		gRunning = false;
	}

	return loadLevels();
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Level* currentLevel)
{
	//std::printf("We have a problem\n");
	GameEvent nextEvent;
	while(!eventQueue.empty())
	{
		nextEvent = eventQueue.front();
		eventQueue.pop();

		if(nextEvent.action == ActionEnum::PLAYER_MOVE_UP)
		{
			currentLevel->updateDirection('w');
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_DOWN)
		{
			currentLevel->updateDirection('s');
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_LEFT)
		{
			currentLevel->updateDirection('a');
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MOVE_RIGHT)
		{
			currentLevel->updateDirection('d');
		}
		else if(nextEvent.action == ActionEnum::PLAYER_MUTE_MUSIC)
		{
			currentLevel->muteSound();
		}
		else if(nextEvent.action == ActionEnum::CHANGE_WALL_ONE)
		{
			currentLevel->changeWallSprites(8);
		}
		else if(nextEvent.action == ActionEnum::CHANGE_WALL_TWO)
		{
			currentLevel->changeWallSprites(9);
		}
		else if(nextEvent.action == ActionEnum::CHANGE_WALL_THREE)
		{
			currentLevel->changeWallSprites(10);
		}
		else if(nextEvent.action == ActionEnum::CHANGE_WALL_DISCO)
		{
			currentLevel->enableDisco();
		}

	}
	currentLevel->updateLevel(deltaTime); //updates The Game Level (only positions and calculations)  -- BKA
}
//This is the main draw function of the program. All calls to the WindowHandler for drawing purposes should originate here.
void draw(Level* currentLevel)
{
	WindowHandler::getInstance().clear();
	WindowHandler::getInstance().drawList(currentLevel->getWalls());
	WindowHandler::getInstance().drawList(currentLevel->getGameObj());
	WindowHandler::getInstance().drawList(currentLevel->getCandy());
	WindowHandler::getInstance().drawList(currentLevel->getGhost());
	TextHandler::getInstance().drawText(currentLevel->getPlayer()->getScore());
	WindowHandler::getInstance().draw(currentLevel->getPlayerObj());
	WindowHandler::getInstance().update();

}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) {
	std::queue<GameEvent> eventQueue; //Main event queue for the program.
	Level* currentLevel = nullptr;
	float nextFrame = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime = 0.0f; //Time since last pass through of the game loop.
	auto clockStart = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop = clockStart;

	currentLevel = init();

	while(gRunning) {
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, currentLevel);

		if(nextFrameTimer >= nextFrame) {
			draw(currentLevel);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}