# Grade: C
Liked:
  The comments
  Multiple sprites
  Music
Disliked:
  Clipping with the walls.

# Assignment 1

## Group creation deadline 2016/9/30 23:59:59
## Hand in deadline 2016/10/7 23:59:59

In this assignment you will be making Pac Man.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

###Ubuntu setup document###
http://goo.gl/9hSkFR

## Required work
1. Finish the code in ```Level::createWalls()``` and ```WindowHandler::draw(ScreenObject* object)```so that walls are added to the level and can be drawn. The walls do not need to have any fancy textures. Filling an appropriate space with color will do.
2. Create a player object.
    1. Use the ```InputHandler``` to read input from **w, a, s, d** and add movement events to the event queue.
    1. Pop events from the event queue in the update function (_main.cpp_) and use them to move Pac Man around.
    1. Implement collision detection so that Pac Man cannot pass through the walls and can pick up orbs.
    1. Load the provided sprite sheet and animate Pac Man as he moves
5. Create and display objects representing orbs/crates/fruit that can be collected by Pac Man and gives him points. (Sprites/textures optional but it must be visible.) These objects should disapear as Pac Man collects them.
6. Modify the mapfile so that it includes data about where spheres that give points should be placed.
5. Create a text handling class.
    1. Use the text handling class to load the font provided.
    1. Use the font to display a score in the top left corner of the screen as Pac Man picks up orbs.
1. Update this document. (See botom.)

## Restrictions
1. No drawing to screen outside of the ```WindowHandler``` (You are free to modify it as much as you want.)
2. You **must** use the SDL2 library for the graphics.
3. Do not use SDL_TTF for the font handling.

## Suggestions for additional work
1. Add enemies (Colliding with the enemies resets Pac Man to the start position.)
    1. Give the enemies a simple AI.
2. Add functionality that let Pac Man exit on one side of the screen and enter on the other.
3. Add sound.
4. Add more levels and a way to switch between them.
5. Implement proper kerning in the font.
6. Etc

##Group comments

###Game instructions###
	
	To start moving, press 'd'.
	Steer pacman with : WASD.
	Switch map modes (only wall graphics) with: 1-3 keys.
	Toggle Disco mode: 4
	Toggle music: m (this starts by default in the right map mode)
	if music doesnt load, try: sudo apt-get install libsdl2-mixer-dev

###Who are the members of the group?###
	The members of this group is:
	1. Aune, Bjørn Kaare						470901
	2. Baasdseth, Kristoffer					470913
	3. Skinstad, Benjamin 'Benski' Normann		470918

###What did you implement and how did you do it? (Individually)###
	
	On most parts of the assginement, we sat together and coded from one machine.
	Therefor, we have contributed equaly on almost all parts of the game,
	with the exeption of fixing the sprite sheet (only the pacman one, not the font sheets).
	We had Benski updating the sprite sheet.
	
	"Sprite sheet, animation, music, varius misc and helping with some logic" -Benski

	Lotsa pseudocode and logic
	Rendered objects on screen
	Implemented a lot of the input for wall-sprite change and mute(mostly copy-paste though)
	Mostly worked as a team on one screen, as stated.
	--KB
  

###What parts if any of the base code did you change and why?###
	
	The update function in main was changed to call the proper update functions in 
	the current level pointer.
	We also added a drawText function in our Windowhandler, to draw from a differnt sprite
	sheet, when dealing with draw calls from the TextHandler.
	fixed clear and timer error
	


###What was the hardest part of this assignment? (Individually)###
	
	/*
	Our differnet attempts at doing collision. We tried many differnt ways of doing it,
	before ending with our current one.
	We also had some trouble with movement, but this was a minor bump in the road.
	Animation went smoothly to implement.
	*/ -- Bjørn

	"the logic behind collision was hard, understadning lazyfoo was not always as easy, for example music"
	Wanted ghost code and movement, no luck with this at the moment. Currently rendered, but static.
	-- Benski

	I hated the collision code, it was a real pain. We still have clipping with the walls, but atleast it works.
	I would've prefered to make pac-man move as if on a grid, but this was easier in theory.
	(Please show me how this can be done, super curious!)
	--KB

###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
	
	Yes. -- Bjørn.
	Though, we hade some problems working all at once, but we solved that by coding on the same machine,
	while all group members worked together on formulating our code. -- Bjørn.
	Furthermore, Benski's computer is shit (when trying to get it to work with the others), so all his code has
	been submitted from either Kristoffer or mine computer. -- Bjørn.

	Yes. -- KB
	Much of Bjørns comments are spot on. My biggest issue was the collision-code, which was wonky to do. 
	We probably "coded ourselves into a corner." The collision-code seems to be alpha and omega.

	yea --benski
	It was enough to make it possible to divide the workload between us, but not so much that we got troubble with handling it.
	I think could have added a lot more features if we didnt get stuck on collision.

###Other comments###


	Might've been easier to understand SDL if we started from scratch, but nice to work on code we hadn't written ourselves.
	Felt like I got some experience in reading and understanding other people's code though,
	this is a nice change from the "start with include 'this'"
	--KB