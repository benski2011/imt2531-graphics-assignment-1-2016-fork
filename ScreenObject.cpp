#include "ScreenObject.h"
//#include <SDL2/SDL_mixer.h>
 
glm::vec2 ScreenObject::getCentrePos() {
	glm::vec2 centre;
	centre.x = size.x / 2;
	centre.y = size.y / 2;
	return centre;
}

glm::vec2 ScreenObject::getPosition() {

	return position;
}


SDL_Rect ScreenObject::getSourceRect()
{
	SDL_Rect rect;

	rect.x = spritePosition.x;
	rect.y = spritePosition.y;
	rect.w = spriteSize.x;
	rect.h = spriteSize.y;
	
	return rect;
}

void ScreenObject::setSpritePos(int x, int y)
{
	spritePosition.x = x;
	spritePosition.y = y;
}

SDL_Rect ScreenObject::getDestRect() {
	SDL_Rect rect;

	rect.x = static_cast<int>(position.x);
	rect.y = static_cast<int>(position.y);
	rect.w = static_cast<int>(size.x);
	rect.h = static_cast<int>(size.y);

	return rect;
}

void ScreenObject::setPosition(glm::vec2 temp)
{
	position.x = temp.x;
	position.y = temp.y;
}

//sets where we shall look for the sprite on the sprite sheet
void ScreenObject::setSpriteSize(glm::vec2 temp)
{
	spriteSize.x = temp.x;
	spriteSize.y = temp.y;
}

//checs for collision
bool ScreenObject::collisionDetection(glm::vec2 temp)
{
	if(temp.x > position.x && temp.x < (position.x+size.x) && temp.y > position.y && temp.y < (position.y+size.y))
	{
		return true;
	}

	return false;
}

//second iterations on collisiondetection, we are trying to expand it. This is not in use as of now
bool ScreenObject::collisionDetection(glm::vec2 temp1, glm::vec2 temp2)
{
	if((temp2.x > position.x && temp2.x < position.x+size.x && temp2.y > position.y && temp2.y < position.y+size.y)||(temp1.x > position.x && temp1.x < position.x+size.x && temp1.y > position.y && temp1.y < position.y+size.y))
	{
		return true;
	}

	return false;
}

glm::vec2 ScreenObject::getSize()
{
	return size;
}

//checks if we are within a small box around the centre of each box
bool ScreenObject::centerCollision(glm::vec2 temp)
{
	glm::vec2 test;
	test.x = position.x + (size.x/2);
	test.y = position.y + (size.y/2);
	if(temp.x > test.x-8 && temp.x < test.x+8 && temp.y > test.y-8 && temp.y < test.x+8)
	{
		return true;
	}

	return false;
}


/*
GHOST MOVEMENT=ON WALL COLLISION, PICK NEW DIRECTION

*/
