#include "PlayerObject.h"

//default constructor for ghosts
PlayerObject::PlayerObject(glm::vec2 pos, glm::vec2 sz) : ScreenObject(pos, sz)
{
	speed = 100;
	direction.x = 1;
	direction.y = 0;
	blit = 0;
	nextTime = 0.f;
	reverse = false;
	score =0;
}

//calculates how pacman moves
void PlayerObject::calcVelocity()
{
	velocity.x = speed * direction.x;
	velocity.y = speed * direction.y;
}

//changes direction vector to our current direction
void PlayerObject::updateDirection(char c)
{
	//char input = c;
	switch (c)
	{
		case 'w':
		{
			
			direction.x = 0;
			direction.y = -1;
			spritePosition.y = 80*2;  	//changes the start sprite for our animation
			break;
		}
		case 's':
		{
			direction.x = 0;
			direction.y = 1;
			spritePosition.y = 80*3;	//changes the start sprite for our animation
			break;
		}
		case 'a':
		{
			direction.x = -1;
			direction.y = 0;
			spritePosition.y = 80*1;	//changes the start sprite for our animation
			break;
		}
		case 'd':
		{
			direction.x = 1;
			direction.y = 0;
			spritePosition.y = 80*0;	//changes the start sprite for our animation
			break;
		}
		default:
		break;
	}

	calcVelocity();
}

//moves pacman around, and updates our animation bliting
void PlayerObject::updatePosition(float deltaTime)
{
	nextTime += deltaTime;
	if(nextTime >= 0.1)
	{

		spritePosition.x = 80*blit;
		if(!reverse)
		{
			blit++;
		}
		else if (reverse)
		{
			blit--;
		}
		nextTime = 0;
		if(blit == 3)
		{
			reverse = true;
		}
		else if (blit ==0)
		{
			reverse = false;
		}
	}
	position.x += velocity.x * deltaTime;
	position.y += velocity.y * deltaTime;
}

//Returns the direction vector -- BKA
glm::vec2 PlayerObject::getDirection()
{
		return direction;
}

//upon collison, this is called. Stops pacman
void PlayerObject::setSpeedZero()
{
	direction.x=0;
	direction.y=0;
	calcVelocity();
}

//updates our score, 
void PlayerObject::updateScore()
{
	score += 10;
}

//returns our score as a string
std::string PlayerObject::getScore()
{
	std::string temp;
	temp = "Score: ";
	temp += std::to_string(score);
	return temp;
}
