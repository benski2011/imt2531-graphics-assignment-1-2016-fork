#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <SDL2/SDL_mixer.h>

#include "ScreenObject.h"
#include "PlayerObject.h"

class Level {
public:
	Level(std::string filepath);

	
	std::vector<ScreenObject>* getWalls();
	std::vector<ScreenObject>* getGameObj();
	std::vector<ScreenObject>* getCandy();
	std::vector<ScreenObject>* getGhost();
	ScreenObject* getPlayerObj();
	PlayerObject* getPlayer();	//this is for calls to the playerObject, where we want functionality in pacman, and not
								//something that we can draw on screen (this comment is badly worded)

	void updateLevel(float dt);					//dt is deltaTime 
	void updateDirection(char c);				//changes direction based on input (input is based on game events)
	float normalizeGridPos(float temp, glm::vec2 test);
	void muteSound();
	void changeWallSprites(int n);
	void enableDisco();

private:
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject> walls;		//holding all wall objects
	std::vector<ScreenObject> candys;		//holding all candy objects
	std::vector<ScreenObject> ghosts; 		//holding all ghosts objects
	PlayerObject* playerObj;				//holding all playerObjects				
	std::vector<ScreenObject> gameObj;		//for holding all objects that are not walls (might create one for each type of object -- BKA)
	Mix_Music* Music;						//pointer to games music

	int currentWalls;
	int spriteSchemeWalls;
	bool musicPlaying;
	bool discoMode;
	float nextTime;

	void loadMap(std::string filepath); //Loads map data from file
	void createWalls();					//does what the name implies, it creates walls
	void createPlayer(); 				//creates our player
	void collisionDetection(); 			//checs for collision
	void createCandy(); 				//creates candy
	void createGhost(); 				//creates ghosts
};