#pragma	once

#include <SDL2/SDL.h>
#include "ScreenObject.h"
#include <glm/glm.hpp>
#include <cstdio>
#include <string>
class PlayerObject : public ScreenObject {
public:

PlayerObject() {};
PlayerObject(glm::vec2 pos, glm::vec2 sz);

void calcVelocity();
void updatePosition(float deltaTime);
void updateDirection(char c);
glm::vec2 getDirection();				//returs a char value of its direction -- BKA
void setSpeedZero();					//stops pacman
void updateScore();						//updates score
std::string getScore();					//returns string to print score on screen

private:
	glm::vec2 velocity;
	glm::vec2 direction;
	int speed;
	int score;

};


